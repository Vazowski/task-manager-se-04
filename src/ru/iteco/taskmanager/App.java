package ru.iteco.taskmanager;


import ru.iteco.taskmanager.bootstrap.Bootstrap;

public class App
{
    public static void main( String[] args )
    {
    	Bootstrap.init();
    }
}
