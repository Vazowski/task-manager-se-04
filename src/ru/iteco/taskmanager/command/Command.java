package ru.iteco.taskmanager.command;

import java.util.LinkedHashMap;
import java.util.Map;

public class Command {

	private static Map<String, String> description;
	
	public static void makeDescription() {
		description = new LinkedHashMap<String, String>();
		
		description.put("help", "  -  show help information");
		description.put("exit", "  -  exit the programm");
    	
		description.put("mp", "  -  merge project");
		description.put("pp", "  -  persist project");
		description.put("fp", "  -  find one project");
		description.put("fallp", "  -  show all projects");
		description.put("rp", "  -  remove one project");
		description.put("rallp", "  -  remove all projects");
    	
		description.put("mt", "  -  merge task");
		description.put("pt", "  -  persist task");
		description.put("ft", "  -  find one task");
		description.put("fallt", "  -  show all task");
		description.put("rt", "  -  remove one task");
		description.put("rallt", "  -  remove all tasks");
    }
	
	public static void showAll() {
		System.out.println();
		for (String key : description.keySet()) {
			if (!key.equals("help"))
			System.out.println(key + description.get(key));
		}
	}
	
	public static boolean keyExist(String key) {
		for (String k : description.keySet()) {
			if (k.equals(key))
				return true;
		}
		System.out.println("Command doesn't exist");
		return false;
	}
}
