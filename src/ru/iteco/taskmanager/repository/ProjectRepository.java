package ru.iteco.taskmanager.repository;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import ru.iteco.taskmanager.entity.Project;

public class ProjectRepository {

	private static Map<String, Project> projectMap;
	
	public ProjectRepository() {
		projectMap = new LinkedHashMap<String, Project>();
	}
	
	public void merge(String name, String description, String uuid) {
		projectMap.put(uuid, new Project(name, description, uuid));
	}

	public List<String> findAll() {
		List<String> result = new ArrayList<>();
		for (Project project : projectMap.values()) {
			result.add(findOne(project.getName()));
		}
		return result;
	}

	public static String findOne(String name) {
		for (Project project : projectMap.values()) {
			if (project.getName().equals(name))
				return project.getUuid();
		}
		return "empty";
	}

	public void remove(String uuid) {
		projectMap.remove(uuid);
	}

	public void removeAll() {
		projectMap.clear();
	}
	
	public Project getProject(String uuid) {
		return projectMap.get(uuid);
	}
	
	public static boolean isExist(String name) {
		for (Project project : projectMap.values()) {
			if (project.getName().equals(name))
				return true;
		}
		return false;
	}
}
