package ru.iteco.taskmanager.repository;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import ru.iteco.taskmanager.entity.Task;

public class TaskRepository {

	private static Map<String, Task> taskMap;
	
	public TaskRepository() {
		taskMap = new LinkedHashMap<String, Task>();
	}
	
	public void merge(String name, String description, String uuid, String projectUuid) {
		taskMap.put(uuid, new Task(name, description, uuid, projectUuid));
	}
	
	public List<String> findAll(String projectUuid) {
		List<String> result = new ArrayList<>();
		for (Task task : taskMap.values()) {
			if (projectUuid.equals(task.getProjectUUID())) {
				result.add(findOne(task.getName()));
			}
		}
		return result;
	}
	
	public String findOne(String name) {
		for (Task task : taskMap.values()) {
			if (task.getName().equals(name))
				return task.getUuid();
		}
		return "empty";
	}

	public void remove(String uuid) {
		taskMap.remove(uuid);
	}
	
	public void removeAll(String projectUuid) {
		Iterator<Task> iterator = taskMap.values().iterator();
	    
	    while(iterator.hasNext()){
	      Task task = iterator.next();
	      if (projectUuid.equals(task.getProjectUUID())) {
	    	  iterator.remove();
	      }
	    }
	}
	
	public Task getTask(String uuid) {
		return taskMap.get(uuid);
	}
	
}
