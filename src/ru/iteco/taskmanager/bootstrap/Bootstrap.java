package ru.iteco.taskmanager.bootstrap;

import java.util.Scanner;

import ru.iteco.taskmanager.command.Command;
import ru.iteco.taskmanager.entity.Entity;
import ru.iteco.taskmanager.entity.Literas;
import ru.iteco.taskmanager.repository.ProjectRepository;
import ru.iteco.taskmanager.repository.TaskRepository;
import ru.iteco.taskmanager.service.ProjectService;
import ru.iteco.taskmanager.service.TaskService;

public class Bootstrap {

	private static Scanner scanner;
	private static String inputCommand;
	
	private static ProjectService projectService = new ProjectService(new ProjectRepository(), new TaskRepository());
	private static TaskService taskService = new TaskService(new TaskRepository());
    
	public static void init() {

		Command.makeDescription();
		
    	System.out.println("Enter command (type help for more information)");
    	
    	while(true) {
    		System.out.print("> ");    		
    		scanner = new Scanner(System.in);
    		inputCommand = scanner.nextLine();
    		if (Command.keyExist(inputCommand)) {
    			execute(inputCommand);
    		}
    	}
	}
	
	private static void execute(String command) {
		switch (command) {
		case Literas.EXIT:
			System.exit(0);
		case Literas.HELP:
			Command.showAll();
			break;
		case Literas.MERGE_PROJECT:
			projectService.merge();
			break;
		case Literas.PERSIST_PROJECT:
			projectService.persist();
			break;
		case Literas.FIND_PROJECT:
			projectService.findOne();
			break;
		case Literas.FIND_ALL_PROJECT:
			projectService.findAll();
			break;
		case Literas.REMOVE_PROJECT:
			projectService.removeOne();
			break;
		case Literas.REMOVE_ALL_PROJECTS:
			projectService.removeAll();
			break;
		case Literas.MERGE_TASK:
			taskService.merge();
			break;
		case Literas.PERSIST_TASK:
			taskService.persist();
			break;
		case Literas.FIND_TASK:
			taskService.findOne();
			break;
		case Literas.FIND_ALL_TASK:
			taskService.findAll();
			break;
		case Literas.REMOVE_TASK:
			taskService.removeOne();
			break;
		case Literas.REMOVE_ALL_TASK:
			taskService.removeAll();
			break;
		}
	}
	
	public static void showInfo(Entity entity) {
		System.out.println(entity.getType() + " name: " + entity.getName());
		System.out.println(entity.getType() + " description: " + entity.getDescription());
		System.out.println(entity.getType() + " begin date: " + entity.getDateBegin());
		System.out.println(entity.getType() + " end date: " + entity.getDateEnd());
		System.out.println(entity.getType() + " UUID: " + entity.getUuid());
	}
}
