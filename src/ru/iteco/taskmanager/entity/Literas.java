package ru.iteco.taskmanager.entity;

public class Literas {

	public static final String HELP = "help"; 
	public static final String EXIT = "exit";
	
	public static final String MERGE_PROJECT = "mp";
	public static final String PERSIST_PROJECT = "pp";
	public static final String FIND_PROJECT = "fp";
	public static final String FIND_ALL_PROJECT = "fallp";
	public static final String REMOVE_PROJECT = "rp";
	public static final String REMOVE_ALL_PROJECTS = "rallp";
	
	public static final String MERGE_TASK = "mt";
	public static final String PERSIST_TASK = "pt";
	public static final String FIND_TASK = "ft";
	public static final String FIND_ALL_TASK = "fallt";
	public static final String REMOVE_TASK = "rt";
	public static final String REMOVE_ALL_TASK = "rallt";
}
