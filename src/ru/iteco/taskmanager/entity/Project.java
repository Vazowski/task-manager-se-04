package ru.iteco.taskmanager.entity;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Project extends Entity{

	private static SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
	
	public Project() {
    	
    }
    
    public Project(String name, String description, String uuid) {
    	this.uuid = uuid;
    	this.name = name;
    	this.description = description;
    	this.dateBegin = dateFormat.format(new Date());
    	this.dateEnd = dateFormat.format(new Date());
    	this.type = "Project";
    }
    
	@Override
	public String toString() {
		return "Name: " + this.name + "\nDescription: " + this.description + "\nDateBegin: " + this.dateBegin + "\nDateEnd: " + this.dateEnd + "\nUUID: " + this.uuid; 
	}
}
