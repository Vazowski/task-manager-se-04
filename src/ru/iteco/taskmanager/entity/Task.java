package ru.iteco.taskmanager.entity;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Task extends Entity{

	private String projectUUID;
    
	private static SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
	
	public Task() {
    	
    }
    
    public Task(String name, String description, String uuid, String projectUuid) {
    	this.uuid = uuid;
    	this.projectUUID = projectUuid;
    	this.name = name;
    	this.description = description;
    	this.dateBegin = dateFormat.format(new Date());
    	this.dateEnd = dateFormat.format(new Date());
    	this.type = "Task";
    }
    
	public String getProjectUUID() {
		return projectUUID;
	}
	
	public void setProjectUUID(String projectUUID) {
		this.projectUUID = projectUUID;
	}
	
	@Override
	public String toString() {
		return "Name: " + this.name + "\nDescription: " + this.description + "\nDateBegin: " + this.dateBegin + "\nDateEnd: " + this.dateEnd + "\nUUID: " + this.uuid + "\nprojectUUID: " + this.projectUUID;
	}
}
