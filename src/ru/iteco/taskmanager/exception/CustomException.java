package ru.iteco.taskmanager.exception;

public class CustomException {

	public static void showError(String source, String text) {
		System.out.println("Exception in " + source + ": " + text);
	}
}
