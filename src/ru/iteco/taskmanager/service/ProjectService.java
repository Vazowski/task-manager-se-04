package ru.iteco.taskmanager.service;

import java.util.List;
import java.util.Scanner;
import java.util.UUID;

import ru.iteco.taskmanager.bootstrap.Bootstrap;
import ru.iteco.taskmanager.entity.ConsoleTemplates;
import ru.iteco.taskmanager.exception.CustomException;
import ru.iteco.taskmanager.repository.ProjectRepository;
import ru.iteco.taskmanager.repository.TaskRepository;

public class ProjectService  {
	
	private String inputName, inputDescription, uuid, findedUuid;
	private Scanner scanner;
	
	private ProjectRepository projectRepo;
	private TaskRepository taskRepo;
	
	public ProjectService(ProjectRepository projectRepo, TaskRepository taskRepo) {
		this.projectRepo = projectRepo;
		this.taskRepo = taskRepo;
		scanner = new Scanner(System.in);
	}
	
	public void merge() {
		System.out.print("Name of project: ");
		inputName = scanner.nextLine();
		System.out.print("Description of project: ");
		inputDescription = scanner.nextLine();
		if (isProjectExist(inputName)) {
			findedUuid = ProjectRepository.findOne(inputName);
			projectRepo.merge(inputName, inputDescription, findedUuid);
		} else {
			uuid = UUID.randomUUID().toString();
			projectRepo.merge(inputName, inputDescription, uuid);
		}
		ConsoleTemplates.isDone();
	}
	
	public void persist() {
		System.out.print("Name of project: ");
		inputName = scanner.nextLine();
		if (!isProjectExist(inputName)) {
			System.out.print("Description of project: ");
			inputDescription = scanner.nextLine();
			uuid = UUID.randomUUID().toString();
			projectRepo.merge(inputName, inputDescription, uuid);
			ConsoleTemplates.isDone();
		} else {
			CustomException.showError("project_service.persist", "project with same name already exist");
		}
	}
	
	public void findOne() {
		System.out.print("Name of project: ");
		inputName = scanner.nextLine();
		findedUuid = ProjectRepository.findOne(inputName);
		if (findedUuid.equals("empty")) {
			Bootstrap.showInfo(projectRepo.getProject(findedUuid)); 
		} else {
			CustomException.showError("project_service.find_one", "no project with same name");
		}
	}
	
	public void findAll() {
		List<String> tempList = projectRepo.findAll();
		if (tempList.size() == 0) {
			ConsoleTemplates.isEmpty();
			return;
		}
		for (int i = 0; i < tempList.size(); i++) {
			System.out.println("[Project " + (i + 1) + "]");
			Bootstrap.showInfo(projectRepo.getProject(tempList.get(i)));
		}
	}
	
	public void removeOne() {
		System.out.print("Name of project: ");
		inputName = scanner.nextLine();
		findedUuid = ProjectRepository.findOne(inputName);
		if (!findedUuid.equals("empty")) {
			taskRepo.removeAll(findedUuid);
			projectRepo.remove(findedUuid);
			ConsoleTemplates.isDone();
		} else {
			ConsoleTemplates.noExist("Project");
		}
	}
	
	public void removeAll() {
		List<String> tempList = projectRepo.findAll();
		for (int i = 0; i < tempList.size(); i++) {
			taskRepo.removeAll(tempList.get(i));
		}
		projectRepo.removeAll();
		ConsoleTemplates.isDone();
	}
	
	public boolean isProjectExist(String projectName) {
		return ProjectRepository.findOne(projectName).equals("empty");
	}
}
