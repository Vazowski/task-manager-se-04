package ru.iteco.taskmanager.service;

import java.util.List;
import java.util.Scanner;
import java.util.UUID;

import ru.iteco.taskmanager.bootstrap.Bootstrap;
import ru.iteco.taskmanager.entity.ConsoleTemplates;
import ru.iteco.taskmanager.exception.CustomException;
import ru.iteco.taskmanager.repository.ProjectRepository;
import ru.iteco.taskmanager.repository.TaskRepository;

public class TaskService {

	private String inputProjectName, inputName, inputDescription, uuid, findedUuid, projectUuid;
	private Scanner scanner;
	
	private TaskRepository taskRepository; 
	
	public TaskService(TaskRepository taskRepository) {
		this.taskRepository = taskRepository;
		scanner = new Scanner(System.in);
	}
	
	public void merge() {
		System.out.print("Name of project: ");
		inputProjectName = scanner.nextLine();
		if (!ProjectRepository.isExist(inputProjectName)) {
			ConsoleTemplates.noExist("Project");
		} else {
			projectUuid = ProjectRepository.findOne(inputProjectName);
			System.out.print("Name of task: ");
			inputName = scanner.nextLine();
			System.out.print("Description of task: ");
			inputDescription = scanner.nextLine();
			if (isTaskExist(inputName)) {
				findedUuid = taskRepository.findOne(inputName);
				taskRepository.merge(inputName, inputDescription, findedUuid, projectUuid);
			} else {
				uuid = UUID.randomUUID().toString();
				taskRepository.merge(inputName, inputDescription, uuid, projectUuid);
			}
			ConsoleTemplates.isDone();
		}
	}
	
	public void persist() {
		System.out.print("Name of project: ");
		inputProjectName = scanner.nextLine();
		if (!ProjectRepository.isExist(inputProjectName)) {
			ConsoleTemplates.noExist("Project");
		} else {
			projectUuid = ProjectRepository.findOne(inputProjectName);
			System.out.print("Name of task: ");
			inputName = scanner.nextLine();
			if (isTaskExist(inputName)) {
				CustomException.showError("task_service.persist", "task with same name already exist");
			} else {
				System.out.print("Description of task: ");
				inputDescription = scanner.nextLine();
				uuid = UUID.randomUUID().toString();
				taskRepository.merge(inputName, inputDescription, uuid, projectUuid);
				ConsoleTemplates.isDone();
			}
		}
	}
	
	public void findOne() {
		System.out.print("Name of project: ");
		inputProjectName = scanner.nextLine();
		if (!ProjectRepository.isExist(inputProjectName)) {
			ConsoleTemplates.noExist("Project");
		} else {
			System.out.print("Name of task: ");
			inputName = scanner.nextLine();
			findedUuid = taskRepository.findOne(inputName);
			if (!findedUuid.equals("empty")) {
				Bootstrap.showInfo(taskRepository.getTask(findedUuid));
			} else {
				CustomException.showError("task_service.find_one", "no task with same name");
			}
		}
	}
	
	public void findAll() {
		System.out.print("Name of project: ");
		inputProjectName = scanner.nextLine();
		if (!ProjectRepository.isExist(inputProjectName)) {
			ConsoleTemplates.noExist("Project");
		} else {
			projectUuid = ProjectRepository.findOne(inputProjectName);
			List<String> tempList = taskRepository.findAll(projectUuid);
			if (tempList.size() == 0) {
				ConsoleTemplates.isEmpty();
				return;
			}
			for (int i = 0; i < tempList.size(); i++) {
				System.out.println("[Task " + (i + 1) + "]");
				Bootstrap.showInfo(taskRepository.getTask(tempList.get(i)));
			}
		}
	}
	
	public void removeOne() {
		System.out.print("Name of project: ");
		inputProjectName = scanner.nextLine();
		if (!ProjectRepository.isExist(inputProjectName)) {
			ConsoleTemplates.noExist("Project");
		} else {
			System.out.print("Name of task: ");
			inputName = scanner.nextLine();
			findedUuid = taskRepository.findOne(inputName);
			if (!findedUuid.equals("empty")) {
				taskRepository.remove(findedUuid);
				ConsoleTemplates.isDone();
			} else {
				ConsoleTemplates.noExist("Task");
			}
		}
	}
	
	public void removeAll() {
		System.out.print("Name of project: ");
		inputProjectName = scanner.nextLine();
		if (!ProjectRepository.isExist(inputProjectName)) {
			ConsoleTemplates.noExist("Project");
		} else {
			projectUuid = ProjectRepository.findOne(inputProjectName);
			taskRepository.removeAll(projectUuid);
			ConsoleTemplates.isDone();
		}
	}
	
	public boolean isTaskExist(String taskName) {
		return !taskRepository.findOne(taskName).equals("empty");
	}
	
}
